--colab/jupyter: https://colab.research.google.com/drive/1j4XdGIU__NYPVpv74vQa9HUOAkxsgUez?usp=sharing

--task1  (lesson7)
-- sqlite3: ������� �������� ������ � �� (sqlite3, project name: task1_7). � ������� table1 �������� 1000 ����� � ���������� ���������� (3 �������, ��� int) �� 0 �� 1000.
-- ����� ��������� ����������� ������������� ���� ���� �������


cur.execute ("""create table table1
as
select floor(random()*(1000-1+1))+1 as t1, floor (random()*(1000-1+1))+1 as t2, floor (random()*(1000-1+1))+1 as t3 
from generate_series(1, 1000)
""") 
df=pd.read_sql_query("select * from table1", conn)
df

from IPython.display import HTML
import plotly.express as px
df = pd.read_sql_query("select * from table1", conn)
fig = px.histogram(df, x="ferst")
fig.show()

import sqlite3
conn =sqlite3.connect('task1_7.db')
c=conn.cursor()
request= 'Create Table table1 (ferst int,id int,last int)'
c.execute(request)
tables=c.fetchall()


--task2  (lesson7)
-- oracle: https://leetcode.com/problems/duplicate-emails/
SELECT Email
FROM Person
GROUP BY Email  
HAVING COUNT(*) > 1


--task3  (lesson7)
-- oracle: https://leetcode.com/problems/employees-earning-more-than-their-managers/
SELECT tb1.Name as Employee
FROM Employee as tb1
LEFT JOIN Employee as tb2
ON tb1.ManagerId = tb2.Id
WHERE tb1.Salary > tb2.Salary



--task4  (lesson7)
-- oracle: https://leetcode.com/problems/rank-scores/
select Score, dense_rank() over(order by Score desc) as 'Rank'
from Scores


--task5  (lesson7)
-- oracle: https://leetcode.com/problems/combine-two-tables/
select FirstName, LastName, City, State
from Person p
left join Address a
on p.PersonId=a.PersonId